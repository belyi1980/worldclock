/**
 * Angular wclock Application Main JS file
 *
 * @author Eugene Belyaev
 * @date Sept 18, 2015
 */

var app = angular.module("wclockApp",[]);

/**
 * Time Controller
 */
app.controller(
    "timeCtrl",
    function($scope, $interval) {
        $scope.timeRegex = /^([012]+[0-9]+)\:+([0-5]+[0-9]+)\:+([0-9]{2})$/;
        $scope.timeTorontoOut = null;
        $scope.timeLondonOut = null;
        $scope.timeSydneyOut = null;
        $scope.interval = false;

        /* private variables */
        $scope._interval = 5;
        $scope._intLink = null;
        $scope._diffLondon = null;
        $scope._diffSydney = null;

        $scope.stopInterval = function() {
            $scope.interval = false;
            $interval.cancel($scope._intLink);
        },

        $scope._intervalCallback = function() {
            $scope.timeTorontoOut = new Date($scope.timeTorontoOut.getTime() + $scope._interval * 1000);
            $scope._adjustLondonTime();
            $scope._adjustSydneyTime();
        },

        $scope._adjustLondonTime = function() {
            if ($scope._diffLondon !== null) {
                $scope.timeLondonOut = new Date($scope.timeTorontoOut.getTime() + $scope._diffLondon * 60 * 60 * 1000);
            } else {
                $scope.timeLondonOut = null;
            }
        },

        $scope._adjustSydneyTime = function() {
            if ($scope._diffSydney !== null) {
                $scope.timeSydneyOut = new Date($scope.timeTorontoOut.getTime() + $scope._diffSydney * 60 * 60 * 1000);
            } else {
                $scope.timeSydneyOut = null;
            }
        },

        $scope.setUserTime = function() {
            $scope.timeTorontoOut = new Date();

            var time = String($scope.timeToronto).match($scope.timeRegex);
            $scope.timeTorontoOut.setHours( parseInt(time[1]) > 23 ? 23 : parseInt(time[1]));
            $scope.timeTorontoOut.setMinutes( parseInt(time[2]) > 59 ? 59 : parseInt(time[2]));
            $scope.timeTorontoOut.setSeconds( parseInt(time[3]) > 60 ? 59 : parseInt(time[3]));

            if (typeof $scope.timeTorontoOut == 'object') {
                $scope.timeToronto = $scope.timeTorontoOut.getHours()+":"+$scope.timeTorontoOut.getMinutes()+":"+$scope.timeTorontoOut.getSeconds();
                $scope.interval = true;
                $scope._intLink = $interval( function(){ $scope._intervalCallback(); }, $scope._interval * 1000);

            }
        }

        $scope.setDiffLondon = function() {
            if (!isNaN($scope.diffLondon)) {
                $scope._diffLondon = $scope.diffLondon; // public `diffLondon` cannot be used as this would make Submit button obsolete
            }
            $scope._adjustLondonTime();
        },

        $scope.setDiffSydney = function() {
            if (!isNaN($scope.diffSydney)) {
                $scope._diffSydney = $scope.diffSydney; // public `diffSydney` cannot be used as this would make Submit button obsolete
            }
            $scope._adjustSydneyTime();
        }
    }
);